if ! pgrep -u "${USER}" ssh-agent > /dev/null; then
    ssh-agent -t 1h > "${XDG_RUNTIME_DIR}/ssh-agent.env"
fi

if [[ ! "${SSH_AUTH_SOCK}" ]]; then
    source "${XDG_RUNTIME_DIR}/ssh-agent.env" > /dev/null
fi

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#    export XDG_SESSION_TYPE=x
    exec sway --unsupported-gpu
fi

