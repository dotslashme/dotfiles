export GTK_THEME=Nordic-darker
export GTK_RC_FILES=/usr/share/themes/Nordic-darker/gtk-2.0/gtkrc
export QT_STYLE_OVERRIDE=kvantum
export JAVA_HOME=/usr/lib/jvm/default
export XDG_CURRENT_DESKTOP=sway

alias k='kubectl'
alias ls='ls -lah --color'
alias mci='mvn clean install'
alias mcp='mvn clean package'
alias gbpl='git branch --merged origin/develop | grep "^\s*feature/" | xargs git branch -d'

