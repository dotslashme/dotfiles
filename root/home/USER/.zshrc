# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Init zplug
source /usr/share/zsh/scripts/zplug/init.zsh

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob nomatch notify append_history inc_append_history share_history hist_save_no_dups hist_find_no_dups dvorak
unsetopt beep
bindkey -v
bindkey '^[[b' beginning-of-line
bindkey '^[[e' end-of-line
bindkey '[[1;5D' backward-word
bindkey '[[1;5C' forward-word

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/cvelin/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

zplug "joshskidmore/zsh-fzf-history-search"
zplug "mdumitru/git-aliases"
zplug "plugins/sudo", from:oh-my-zsh
zplug "greymd/docker-zsh-completion"

if ! zplug check; then
    zplug install
fi

zplug load
